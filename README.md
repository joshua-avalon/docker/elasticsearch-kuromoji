# [elasticsearch-kuromoji][project]

[![License][license_md]][license]
[![GitLab CI][gitlab_ci]][gitlab]
[![Docker Pull][docker_pull]][docker]
[![Docker Star][docker_star]][docker]
[![Docker Size][docker_size]][docker]
[![Docker Layer][docker_layer]][docker]

[Elasticsearch][elasticsearch] with [kuromoji plugin][kuromoji].

## Usage

Please refer to Elasticsearch.

[docker]: https://hub.docker.com/r/joshava/elasticsearch-kuromoji
[docker_pull]: https://img.shields.io/docker/pulls/joshava/elasticsearch-kuromoji.svg
[docker_star]: https://img.shields.io/docker/stars/joshava/elasticsearch-kuromoji.svg
[docker_size]: https://img.shields.io/microbadger/image-size/joshava/elasticsearch-kuromoji.svg
[docker_layer]: https://img.shields.io/microbadger/layers/joshava/elasticsearch-kuromoji.svg
[license]: https://gitlab.com/joshua-avalon/docker/elasticsearch-kuromoji/blob/master/LICENSE
[license_md]: https://img.shields.io/badge/license-Apache--2.0-green.svg
[gitlab]: https://gitlab.com/joshua-avalon/docker/elasticsearch-kuromoji/pipelines
[gitlab_ci]: https://gitlab.com/joshua-avalon/docker/elasticsearch-kuromoji/badges/master/pipeline.svg
[project]: https://gitlab.com/joshua-avalon/docker/elasticsearch-kuromoji
[elasticsearch]: https://www.elastic.co/
[kuromoji]: https://www.elastic.co/guide/en/elasticsearch/plugins/current/analysis-kuromoji.html
