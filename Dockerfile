ARG ELASTIC_VERSION

FROM docker.elastic.co/elasticsearch/elasticsearch:$ELASTIC_VERSION

RUN /usr/share/elasticsearch/bin/elasticsearch-plugin install analysis-kuromoji
